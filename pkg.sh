#!/bin/sh
set -eu

file_name=oh-no-stolen-reports.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths \
    $file_name \
    onsr.esp \
    CHANGELOG.md \
    LICENSE \
    README.md \
    version.txt

sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt
