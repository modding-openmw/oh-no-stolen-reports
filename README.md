# Oh No, Stolen Reports!

Randomizes the location of Ajira's stolen reports in the related Mage's Guild quest.

Includes automatic, optional support for [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) with double the randomized locations! (Requires BCOM 2.9.7 or newer!)

#### Credits

**Author**: johnnyhostile

**Special Thanks**:

* MTR for making [mtrDwemerPuzzleBoxRandomizer](https://www.nexusmods.com/morrowind/mods/49781), which I studied in order to make this
* Danae and Merlord for making [Ajira's Alchemy Reports Randomised](https://www.nexusmods.com/morrowind/mods/47550), which inspired this
* Benjamin Winger for making [Delta Plugin](https://gitlab.com/bmwinger/delta-plugin/)
* RandomPal for making [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231)
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/oh-no-stolen-reports/)

[Nexus Mods](https://www.nexusmods.com/morrowind/mods/53046)

[Source on GitLab](https://gitlab.com/modding-openmw/oh-no-stolen-reports)

#### Installation

[This short video](https://www.youtube.com/watch?v=xzq_ksVuRgc) demonstrates the whole process in under two minutes.

1. Download the zip from a link above
1. Extract the contents to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Randomizers\oh-no-stolen-reports

        # Linux
        /home/username/games/OpenMWMods/Randomizers/oh-no-stolen-reports

        # macOS
        /Users/username/games/OpenMWMods/Randomizers/oh-no-stolen-reports
1. Add the appropriate data path to your `openmw.cfg` file, for example:

        data="C:\games\OpenMWMods\Randomizers\oh-no-stolen-reports"
1. Enable the mod plugin via OpenMW-Launcher, or add this to `openmw.cfg` ([official OpenMW documentation](https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html#install)):

        content=onsr.esp
1. Note: if [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) is installed the optional support for it will automatically be enabled!

#### Known Issues

* I didn't change any dialogue, so certain lines that discuss the papers' hiding places will be off
* If you're playing without BCOM, you'll see this in the `openmw.log` file, it's normal and can be disregarded:
```
[16:50:44.132 I] Warning: onsrMOMWscroll1 line 17, column 19 (BCoM_Active): Parsing a non-variable string as a number: 0
[16:50:44.132 I] Warning: onsrMOMWscroll1 line 51, column 21 (BCOM_Active): Parsing a non-variable string as a number: 0
[16:50:44.132 I] Warning: onsrMOMWscroll1 line 59, column 21 (BCOM_Active): Parsing a non-variable string as a number: 0
[16:50:44.133 I] Warning: onsrMOMWscroll2 line 17, column 19 (BCoM_Active): Parsing a non-variable string as a number: 0
[16:50:44.133 I] Warning: onsrMOMWscroll2 line 51, column 21 (BCOM_Active): Parsing a non-variable string as a number: 0
[16:50:44.133 I] Warning: onsrMOMWscroll2 line 59, column 21 (BCOM_Active): Parsing a non-variable string as a number: 0
```

#### How To Test This Mod

If you're interested in jumping right in and seeing this mod in action, you can do the following:

* Use the OpenMW feature to start a test character and spawn into `Balmora, Guild of Mages` (or the CSSE/MWSE equivalent)
* Press \` to bring down the console
* Enter `journal MG_Flowers 10` and press enter
* The reports will now be moved to their randomized locations

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/oh-no-stolen-reports/-/issues/new) for bug reports or feature requests
* Email: `johnnyhostile at modding-openmw dot com`
* Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/53046?tab=posts)
