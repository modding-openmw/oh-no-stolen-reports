## Oh No, Stolen Reports! Changelog

#### Version 2.0

* **Vastly** simplified implementation; the mod now works by modifying the scripts already placed on Ajira's scrolls.
* Fixed a bug that caused the scrolls to sometimes not appear.
* Automatically detect when [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) is installed and enable the extra report spawn points. This means that only one plugin works for BCOM and non-BCOM setups (Requires BCOM 2.9.7 or newer).

[Download Link](https://gitlab.com/modding-openmw/oh-no-stolen-reports/-/packages/23497423) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53046)

#### Version 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/oh-no-stolen-reports/-/packages/15199210) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53046)
